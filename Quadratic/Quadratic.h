#ifndef QUADRATIC_H
#define QUADRATIC_H

#include <QWidget>
#include <QtWidgets>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QPushButton>
#include <QBoxLayout>


class Quadratic : public QWidget {
	Q_OBJECT
public:
	Quadratic(QWidget* parent = nullptr);
	
private:
	QLineEdit* m_inputX;
	QLineEdit* m_inputY;
	QLineEdit* m_inputZ;
	
	QPushButton* m_pushButton;
	
	QLabel* m_labelForInput;
	

public slots:
	void calculateValues();
	
signals:
	void summChanged(QString);

};

#endif // QUADRATIC_H