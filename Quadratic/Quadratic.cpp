#include "Quadratic.h"
#include <QDebug>
#include <QtWidgets>
#include <cmath>

Quadratic::Quadratic(QWidget *parent) : QWidget(parent){
	m_inputX = new QLineEdit;
	m_inputY = new QLineEdit;
	m_inputZ = new QLineEdit;

	m_pushButton = new QPushButton;

	m_labelForInput = new QLabel;
	m_labelForInput->setMinimumSize( 100, 20);
	m_labelForInput->setText("0");
	
	m_inputX->setMinimumSize(50, 20);
	m_inputY->setMinimumSize(50, 20);
	m_inputZ->setMinimumSize(50, 20);
	
	m_pushButton->setMinimumSize( 140,20);
	
	m_inputX->setPlaceholderText("Input X");
	m_inputY->setPlaceholderText("Input Y");
	m_inputZ->setPlaceholderText("Input Z");
	m_pushButton->setText("Result");

	QGridLayout* layOutNew = new QGridLayout;
	
	QBoxLayout* layOutInputs = new QBoxLayout(QBoxLayout::LeftToRight);
	layOutInputs->addWidget( m_inputX);
	layOutInputs->addStretch(1);
	layOutInputs->addWidget( m_inputY);
	layOutInputs->addStretch(1);
	layOutInputs->addWidget( m_inputZ);
	
	layOutNew->addLayout(layOutInputs, 1,1);
	
	QBoxLayout* layOutSecondLine = new QBoxLayout(QBoxLayout::LeftToRight);
	layOutSecondLine->addWidget( m_labelForInput);
	layOutSecondLine->addStretch(1);
	layOutSecondLine->addWidget( m_pushButton);
	
	layOutNew->addLayout(layOutSecondLine, 2,1);

	setLayout( layOutNew);

	QObject::connect( this->m_pushButton, SIGNAL(clicked()), this, SLOT(calculateValues()));
	QObject::connect( this, SIGNAL(summChanged(QString)), this->m_labelForInput, SLOT(setText( QString)));
}

void Quadratic::calculateValues(){

	double x = m_inputX->text().toDouble();
	double y = m_inputY->text().toDouble();
	double z = m_inputZ->text().toDouble();
	
	double delta, solution1, solution2;
	
	delta = y*y - 4*x*z;

	if (delta > 0 ){
		solution1 = (-y + sqrt(delta)) / (2*x);
		solution2 = (-y - sqrt(delta)) / (2*x);
		
		QString text("There are 2 solutions: %1 or %2");
		text = text.arg(solution1).arg( solution2);
		
		emit summChanged( text);
	} else if (delta == 0){
		solution1 = (-y) / (2*x);
		QString text("There is 1 solution: %1");
		text = text.arg(solution1);
		emit summChanged( text);
	} else {
		QString text("There is no solution.");
		emit summChanged( text);
	}
	
}
