#include <QtWidgets>
#include "Quadratic.h"

int main(int argc, char *argv[])
{
	QApplication app( argc, argv);

	Quadratic layerInput;
	layerInput.setWindowTitle( "Test App");
	layerInput.resize(600, 200);
	layerInput.show();

	return app.exec();
}
